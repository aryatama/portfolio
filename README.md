# Ary Pratama Putra

## About me
Hi I'm Ary, I currently focused on learning in JavaScript, but exposed in some aspects of mobile app development including web development (front-end), version management with git. I always keep to learning about mobile app development with the latest technologies required to work with it.

## Latest Project
* **[Sayembara](https://gitlab.com/aryatama/portfolio/-/tree/Sayembara)** (React Native)
* **[MovieHub](https://gitlab.com/aryatama/portfolio/-/tree/MovieApp)** (React Native)
* and other (React.js) \
**Check at branch on this repository (Portfolio)*

##### Portfolio Website
* <https://aryatama.github.io/>





